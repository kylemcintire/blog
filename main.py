import os
import webapp2
import jinja2
import httplib2
import pprint

from apiclient.discovery import build
from apiclient.http import MediaFileUpload
from oauth2client.client import OAuth2WebServerFlow


template_dir = os.path.join(os.path.dirname(__file__), 'templates')
jinja_env = jinja2.Environment(loader=jinja2.FileSystemLoader(template_dir),
                               autoescape=True)

CLIENT_ID = 'my client id'
CLIENT_SECRET = "my client secret"
REDIRECT_URI = 'http://blog-mcintire.appspot.com/authorized'
OAUTH_SCOPE = 'https://www.googleapis.com/auth/drive'

flow = OAuth2WebServerFlow(CLIENT_ID, CLIENT_SECRET, OAUTH_SCOPE,
                           redirect_uri=REDIRECT_URI)
uri = flow.step1_get_authorize_url(flow.redirect_uri)

FILENAME = 'document.txt'


class Handler(webapp2.RequestHandler):
    def write(self, *a, **kw):
        self.response.out.write(*a, **kw)

    @staticmethod
    def render_str(template, **params):
        t = jinja_env.get_template(template)
        return t.render(params)

    def render(self, template, **kw):
        self.write(self.render_str(template, **kw))


class MainHandler(Handler):
    def get(self):
        self.redirect(uri)


class AuthorizedHandler(Handler):
    def get(self):
        self.response.write('Success')

        credentials = flow.step2_exchange(self.request.get('code'))
        http = httplib2.Http()
        http = credentials.authorize(http)

        drive_service = build('drive', 'v2', http=http)

        # Insert a file
        media_body = MediaFileUpload(FILENAME, mimetype='text/plain', resumable=True)
        body = {
            'title': 'My document',
            'description': 'A test document',
            'mimeType': 'text/plain'
        }

        my_file = drive_service.files().insert(body=body, media_body=media_body).execute()

        self.response.write('upload?')


app = webapp2.WSGIApplication([
                              ('/', MainHandler),
                              ('/authorized', AuthorizedHandler)],
                              debug=True)
